package uis.edu.sprintaton;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SprintatonApplication {

	public static void main(String[] args) {
		SpringApplication.run(SprintatonApplication.class, args);
	}

}
