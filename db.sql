-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema mt2022_c3
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mt2022_c3
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mt2022_c3` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci ;
USE `mt2022_c3` ;

-- -----------------------------------------------------
-- Table `mt2022_c3`.`users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mt2022_c3`.`users` ;

CREATE TABLE IF NOT EXISTS `mt2022_c3`.`users` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `password` VARCHAR(255) CHARACTER SET 'utf8mb3' NOT NULL,
  `username` VARCHAR(255) CHARACTER SET 'utf8mb3' NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `mt2022_c3`.`products`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mt2022_c3`.`products` ;

CREATE TABLE IF NOT EXISTS `mt2022_c3`.`products` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` TEXT CHARACTER SET 'utf8mb3' NOT NULL,
  `price` DECIMAL(10,0) NOT NULL DEFAULT '0',
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_at` TIMESTAMP NULL DEFAULT NULL,
  `users_id` INT NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_products_users_idx` (`users_id` ASC) VISIBLE,
  CONSTRAINT `fk_products_users`
    FOREIGN KEY (`users_id`)
    REFERENCES `mt2022_c3`.`users` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
